/*
 * Copyright © 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#include "syslog_log.h"

#include <cstdarg>
#include <string>

#include <syslog.h>

repowerd::SyslogLog::SyslogLog(repowerd::LogLevel threshold) : repowerd::Log::Log(threshold)
{
    openlog("repowerd", LOG_PID, LOG_DAEMON);
}

repowerd::SyslogLog::~SyslogLog()
{
    closelog();
}

void repowerd::SyslogLog::logOutput(repowerd::LogLevel level, char const* tag, char const* format, va_list ap)
{
    std::string const format_str = std::string{tag} + ": " + format;

    int priority;
    switch (level) {
    case LogLevel::Warning:
        priority = LOG_WARNING;
        break;
    case LogLevel::Info:
        priority = LOG_INFO;
        break;
    default:
        priority = LOG_DEBUG;
    }
    vsyslog(priority, format_str.c_str(), ap);
}
