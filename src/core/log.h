/*
 * Copyright © 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#pragma once

#include <cstdarg>

namespace repowerd
{

enum class LogLevel { Debug, Info, Warning };

class Log
{
public:
    virtual ~Log() = default;

    virtual void logOutput(LogLevel level, char const* tag, char const* format, va_list ap) = 0;

    void vlogLevel(LogLevel level, char const* tag, char const* format, va_list ap);
    void logLevel(LogLevel level, char const* tag, char const* format, ...)
        __attribute__ ((format (printf, 4, 5)));
    void log(char const* tag, char const* format, ...)
        __attribute__ ((format (printf, 3, 4)));
    void logDebug(char const* tag, char const* format, ...)
        __attribute__ ((format (printf, 3, 4)));
    void logInfo(char const* tag, char const* format, ...)
        __attribute__ ((format (printf, 3, 4)));
    void logWarning(char const* tag, char const* format, ...)
        __attribute__ ((format (printf, 3, 4)));

protected:
    LogLevel m_threshold;

    Log(LogLevel threshold) : m_threshold(threshold) {};
    Log(Log const&) = delete;
    Log& operator=(Log const&) = delete;
};

}
