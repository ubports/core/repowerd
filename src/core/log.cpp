/*
 * Copyright © 2016 Canonical Ltd.
 * Copyright © Guido Berhoerster
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>,
 *              Guido Berhoerster <guido+ubports@berhoerster.name>
 */

#include "log.h"

#include <cstdarg>

void repowerd::Log::vlogLevel(LogLevel level, char const* tag, char const* format, va_list ap)
{
    if (level < m_threshold)
        return;

    logOutput(level, tag, format, ap);
}

void repowerd::Log::logLevel(repowerd::LogLevel level, char const* tag, char const* format, ...)
{
    va_list ap;
    va_start(ap, format);

    vlogLevel(level, tag, format, ap);

    va_end(ap);
}

void repowerd::Log::log(char const* tag, char const* format, ...)
{
    va_list ap;
    va_start(ap, format);

    vlogLevel(LogLevel::Info, tag, format, ap);

    va_end(ap);
}

void repowerd::Log::logDebug(char const* tag, char const* format, ...)
{
    va_list ap;
    va_start(ap, format);

    vlogLevel(LogLevel::Debug, tag, format, ap);

    va_end(ap);
}

void repowerd::Log::logInfo(char const* tag, char const* format, ...)
{
    va_list ap;
    va_start(ap, format);

    vlogLevel(LogLevel::Info, tag, format, ap);

    va_end(ap);
}

void repowerd::Log::logWarning(char const* tag, char const* format, ...)
{
    va_list ap;
    va_start(ap, format);

    vlogLevel(LogLevel::Warning, tag, format, ap);

    va_end(ap);
}
