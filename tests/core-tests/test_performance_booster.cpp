/*
 * Copyright © 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#include "acceptance_test.h"
#include "mock_performance_booster.h"

#include <gmock/gmock.h>

namespace rt = repowerd::test;

namespace
{

struct APerformanceBooster : rt::AcceptanceTest
{
    void expect_suspend_mode()
    {
        EXPECT_CALL(*config.the_mock_performance_booster(), 
            set_performance_mode(repowerd::PerformanceBooster::suspend));
    }

    void expect_screen_off_mode()
    {
        EXPECT_CALL(*config.the_mock_performance_booster(), 
            set_performance_mode(repowerd::PerformanceBooster::screen_off));
    }

    void expect_sustained_mode()
    {
        EXPECT_CALL(*config.the_mock_performance_booster(), 
            set_performance_mode(repowerd::PerformanceBooster::sustained));
    }

    void expect_interactive_mode()
    {
        EXPECT_CALL(*config.the_mock_performance_booster(),
            set_performance_mode(repowerd::PerformanceBooster::interactive));
    }
};

}

TEST_F(APerformanceBooster, interactive_then_sustained_when_display_turns_on)
{
    expect_interactive_mode();
    turn_on_display();

    expect_sustained_mode();
    advance_time_by(std::chrono::milliseconds{3000});
}

TEST_F(APerformanceBooster, screen_off_then_suspend_when_display_turns_off)
{
    turn_on_display();

    expect_screen_off_mode();
    turn_off_display();

    expect_suspend_mode();
    advance_time_by(std::chrono::milliseconds{4000});
}

TEST_F(APerformanceBooster, interactive_on_user_activity_changing_power_state)
{
    turn_on_display();

    expect_sustained_mode();
    advance_time_by(std::chrono::milliseconds{3000});

    expect_interactive_mode();
    perform_user_activity_changing_power_state();
}

TEST_F(APerformanceBooster, interactive_on_user_activity_extending_power_state)
{
    turn_on_display();

    expect_sustained_mode();
    advance_time_by(std::chrono::milliseconds{3000});

    expect_interactive_mode();
    perform_user_activity_extending_power_state();
}

TEST_F(APerformanceBooster, interactive_continue_on_as_user_interacts)
{
    turn_on_display();

    expect_sustained_mode();
    advance_time_by(std::chrono::milliseconds{3000});

    expect_interactive_mode();
    perform_user_activity_extending_power_state();

    expect_interactive_mode();
    advance_time_by(std::chrono::milliseconds{1500});
    perform_user_activity_extending_power_state();

    expect_sustained_mode();
    advance_time_by(std::chrono::milliseconds{3000});
}
